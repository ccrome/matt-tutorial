import numpy as np
import numpy.random as random
import pygame
from pygame.event import Event


nrows = 4
ncols = 6
squaresize=50

def make_board(nrows, ncols):
    boardsize = (nrows, ncols)
    gameboard = np.array(list(range(nrows*ncols)))
    gameboard[-1] = -1
    gameboard = np.reshape(gameboard, (nrows, ncols))
    #return gameboard
    keys = [pygame.K_DOWN, pygame.K_UP, pygame.K_LEFT, pygame.K_RIGHT]
    for i in range(1000):
        update_gameboard(gameboard, random.choice(keys))
    return gameboard

BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GRAY = (200, 200, 200)

def display_gameboard(screen, board, squares, squaresize):
    for row in range(board.shape[0]):
        for col in range(board.shape[1]):
            square_value = board[row, col]
            if square_value >= 0:
                number = squares[square_value]
                screen.blit(number, (squaresize*col, squaresize*row))


def update_gameboard(board: np.ndarray, key):
    # First, find the zero:
    nrow, ncol = board.shape
    zrow, zcol = np.unravel_index(np.argmin(board+1), board.shape)
    if key == pygame.K_LEFT:
        if zcol < ncol-1:
            board[zrow, zcol] = board[zrow, zcol+1]
            board[zrow, zcol+1] = -1
    elif key == pygame.K_RIGHT:
        if zcol > 0:
            board[zrow, zcol] = board[zrow, zcol-1]
            board[zrow, zcol-1] = -1
    elif key == pygame.K_UP:
        if zrow < nrow-1:
            board[zrow, zcol] = board[zrow+1, zcol]
            board[zrow+1, zcol] = -1
    elif key == pygame.K_DOWN:
        if zrow > 0:
            board[zrow, zcol] = board[zrow-1, zcol]
            board[zrow-1, zcol] = -1
    
def game_won(gameboard):
    gb = np.reshape(np.array(gameboard), (gameboard.shape[0]*gameboard.shape[1]))
    winner = True
    gb[-1] = len(gb)-1
    for i, n in enumerate(gb):
        if i != n:
            winner = False
            break
    return winner
 

def display_winner(screen):
    font = pygame.font.SysFont(None, 50)
    winner = font.render(f"WINNER", False, GREEN)
    screen.blit(winner, (0, 0))

def make_squares(filename, nrows, ncols, squaresize, show_number=False):
    img = pygame.image.load(filename)
    img = pygame.transform.scale(img, (ncols*squaresize, nrows*squaresize))
    squares = []
    i = 0
    font = pygame.font.SysFont(None, 25)
    for row in range(nrows):
        for col in range(ncols):
            s = pygame.Surface((squaresize, squaresize))
            squares.append(s)
            r = pygame.Rect(squaresize*col, squaresize*row, squaresize, squaresize)
            s.blit(img, (0, 0), area=r)
            if show_number:
                s.blit(font.render(f"{i}", (0, 0), BLUE), (0, 0))
            i += 1
    squares[-1].fill(RED)
    return squares, img


def main():
    board = make_board(nrows, ncols)
    pygame.init()
    logo = pygame.image.load("images/logo.png")
    pygame.display.set_icon(logo)
    pygame.display.set_caption("15-puzzle")
    screen = pygame.display.set_mode((squaresize*ncols*2, squaresize*nrows))
    running = True
    squares, img = make_squares("images/mushroom.jpg", nrows, ncols, squaresize)
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                update_gameboard(board, event.key)
        screen.fill((0, 0, 0))
        display_gameboard(screen, board, squares, squaresize)
        if game_won(board):
            display_winner(screen)
        screen.blit(img, (squaresize*ncols, 0))
        pygame.display.update()




if __name__ == "__main__":
    main()
