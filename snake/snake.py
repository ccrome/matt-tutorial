import numpy as np
import numpy.random as random
import pygame
from pygame.event import Event
from dataclasses import dataclass

GRID = (40, 0, 0)
OVERLAY = (0, 80, 80)
HEAD = (0, 0, 200)
TAIL = (0, 200, 0)

@dataclass
class Snake:
    nx: int
    ny: int
    squaresize: int
    running: bool = True
    screen: pygame.Surface = None
    period: float = 50
    direction: np.ndarray = np.array((1, 0))
    head: np.ndarray = None
    tail = list()

    def __post_init__(self):
        pygame.init()
        logo = pygame.image.load("images/logo.png")
        pygame.display.set_icon(logo)
        pygame.display.set_caption("snake")
        gridsize = (self.squaresize*self.nx, self.squaresize*self.ny)
        self.screen = pygame.display.set_mode(gridsize)
        self.grid = pygame.Surface(gridsize)
        self.draw_grid(self.grid)
        self.font = pygame.font.SysFont(None, 50)
        self.head = np.array((random.randint(0, self.nx-1), random.randint(0, self.ny-1)))

    def draw_time(self):
        now = pygame.time.get_ticks()
        t = self.font.render(f"{now}", False, OVERLAY)
        self.screen.blit(t, (0, 0))

    def run(self):
        next_time = pygame.time.get_ticks() + self.period
        while self.still_running():
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                if event.type == pygame.KEYDOWN:
                    self.update_gameboard(event.key)
            t = pygame.time.get_ticks()
            if t > next_time:
                self.update_board()
                next_time = t + self.period
                self.screen.fill((0, 0, 0))
                self.screen.blit(self.grid, (0, 0))
                self.draw_snake()
                self.draw_time()
                pygame.display.update()

    def update_gameboard(self, key):
        if key == pygame.K_UP:
            self.direction = np.array((0, -1))
        elif key == pygame.K_DOWN:
            self.direction = np.array((0, 1))
        elif key == pygame.K_LEFT:
            self.direction = np.array((-1, 0))
        elif key == pygame.K_RIGHT:
            self.direction = np.array((1, 0))

    def update_board(self):
        self.head = self.head + self.direction
        self.tail.append(self.head)

    def draw_snake(self):
        ss = self.squaresize
        for p in self.tail:
            x, y = list(p * ss)
            pygame.draw.rect(self.screen, TAIL, (x, y, ss, ss))
        x, y = list(self.tail[-1] * ss)
        pygame.draw.rect(self.screen, HEAD, (x, y, ss, ss))

    def still_running(self):
        if self.head[0] < 0:
            return False
        if self.head[0] >= self.nx:
            return False
        if self.head[1] < 0:
            return False
        if self.head[1] >= self.ny:
            return False
        return True

    def draw_grid(self, surface):
        ss = self.squaresize
        sz = self.screen.get_size()
        for i in range(self.nx):
            x = ss*i
            pygame.draw.line(surface, GRID, (x, 0), (x, sz[1]), width=2)
        for i in range(self.ny):
            y = ss*i
            pygame.draw.line(surface, GRID, (0, y), (sz[0], y), width=2)



def main():
    snake = Snake(nx=40, ny=30, squaresize=10)
    snake.run()

if __name__ == "__main__":
    main()
